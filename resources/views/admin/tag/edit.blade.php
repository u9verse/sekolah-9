@extends('admin.layout.master')
@section('title')
    Halaman Update film ({{$tag->id}})
@endsection
@section('content')
<div class="m-5">
    <h2>Update Data</h2>
            <form action="/tag/{{$tag->id}} " method="POST">
                @csrf
                @method('put')
                <div class="form-group">
                    <label for="nama">Nama Tag</label>
                    <input type="text" class="form-control" name="nama" id="title" value="{{$tag->nama}}">
                    @error('nama')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                
                
                <button type="submit" class="btn btn-primary">Update</button>
            </form>
</div>
@endsection