@extends('admin.layout.master')
@section('title')
    Halaman Tambah Tag
@endsection
@section('content')
    <div class="m-5">
        <h2>Tambah Tag</h2>
            <form action="/tag" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <label for="nama">Nama Tag</label>
                    <input type="text" class="form-control" name="nama" id="title" placeholder="Masukkan Title">
                    @error('nama')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <button type="submit" class="btn btn-primary">Tambah</button>
            </form>
    </div>
@endsection