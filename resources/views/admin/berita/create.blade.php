@extends('admin.layout.master')
@section('title')
    Halaman Tambah Berita
@endsection
@section('content')
        <div class="m-5">
            <h2>Tambah Data</h2>
            <form action="/admnews" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <label for="users_id">Penulis</label>
                    <h3>{{ Auth::users()->name }}</h3>
                    <input type="hidden" name="users_id" value="{{ Auth::users()->id }}">
                </div>
                <div class="form-group">
                    <label for="judul">judul Berita</label>
                    <input type="text" class="form-control" name="judul" id="title" placeholder="Masukkan Title">
                    @error('judul')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="isi">Isi</label>
                    <br>
                    <textarea name="isi" id="" class="form-control" cols="30" rows="10"></textarea>
                    @error('isi')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="gambar">Gambar</label><br>
                    <input type="file" name="gambar" id="body">
                    @error('gambar')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                
                <div class="form-group">
                    <label for="tag_id">Tag</label>
                    <select name="tag_id" class="form-control" id="">
                        <option value="">Pilih tag</option>
                        @foreach ($tag as $item)
                            <option value="{{$item->id}} ">{{$item->nama}}</option>
                        @endforeach
                    </select>
                    @error('tag_id')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <button type="submit" class="btn btn-primary">Tambah</button>
            </form>
        </div>
@endsection