@extends('admin.layout.master')
@section('title')
    Halaman Update Berita ({{$berita->id}}) {{$berita->judul}}
@endsection
@section('content')
<div class="m-5">
    <h2>Edit Data</h2>
    <a href="/admnews" class="btn btn-info">Back</a>
    <div class="m-5">
        <form action="/admnews/{{$berita->id}}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('put')
            <div class="form-group">
                <label for="users_id">Penulis</label>
                <h3>{{ Auth::users()->name }}</h3>
                <input type="hidden" name="users_id" value="{{ Auth::users()->id }}">
            </div>
            <div class="form-group">
                <label for="judul">judul Berita</label>
                <input type="text" class="form-control" name="judul" id="title" value="{{$berita->judul}} ">
                @error('judul')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="isi">Isi</label>
                <br>
                <textarea name="isi" id="" class="form-control" cols="30" rows="10">{{$berita->isi}} </textarea>
                @error('isi')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <div class="col">
                    <img src="{{asset('gambar/' . $berita->gambar)}}" class="rounded" style="max-height: 200px" alt="">
                </div>
                <div class="col">
                    <label for="gambar">gambar</label><br>
                    <input type="file" name="gambar" id="body">
                    @error('gambar')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
            </div>

            <div class="form-group">
                <label for="tag_id">Tag</label>
                <select name="tag_id" class="form-control" id="">
                    @foreach ($tag as $item)
                    @if ($item->id == $berita->tag_id)
                        <option value="{{$item->id}}" selected>{{$item->nama}}</option>
                    @else
                        <option value="{{$item->id}} ">{{$item->nama}}</option>
                    @endif
                    @endforeach
                </select>
                @error('tag_id')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>

            <button type="submit" class="btn btn-primary">Update</button>
        </form>
    </div>
@endsection