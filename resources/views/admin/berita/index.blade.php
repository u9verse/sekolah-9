@extends('admin.layout.master')
@section('title')
    Halaman List Berita
@endsection
@section('content')
    <div class="m-4">
        <a href="/admnews/create" class="btn btn-primary">Tambah</a>
            <div class="row">
                @foreach ($berita as $item)
                    <div class="col-4 mt-5">
                        <div class="card" style="width: 18rem;">
                            <img src="{{asset('gambar/' . $item->gambar )}}" class="card-img-top rounded" alt="...">
                            <div class="card-body">
                            <h5 class="card-title">{{$item->judul}} </h5>
                            <p class="card-text">{{Str::limit($item->isi,50)}} </p>
                            <p><span class="badge badge-dark">{{$item->tag->nama}}</span></p>
                            <a href="/admnews/{{$item->id}}" class="btn btn-primary">Detail</a>
                            <a href="/admnews/{{$item->id}}/edit" class="btn btn-warning">Edit</a>
                            <form action="/admnews/{{$item->id}}" method="post">
                                @csrf
                                @method('delete')
                                <input type="submit" value="Delete" class="btn btn-danger">
                            </form>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
    </div>
@endsection