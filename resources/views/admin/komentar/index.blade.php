@extends('admin.layout.master')
@section('title')
    Halaman List Komentar
@endsection
@section('content')
@extends('admin.layout.master')
@section('title')
    Halaman List Komentar
@endsection
@section('content')
    <div class="m-4">
        <a href="/komentar/create" class="btn btn-primary">Tambah</a>
            <table class="table">
                <thead class="thead-light">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">ID</th>
                    <th scope="col">Nama</th>
                    <th scope="col">Berita</th>
                    <th scope="col">Isi Komentar</th>
                    <th scope="col">Actions</th>
                </tr>
                </thead>
                <tbody>
                    @forelse ($komentar as $key=>$value)
                        <tr>
                            <td>{{$key + 1}}</td>
                            <td>{{$value->id}}</td>
                            <td>{{$value->users->name}}</td>
                            <td>{{$value->berita->judul}}</td>
                            <td>{{$value->isi}}</td>
                            <td>
                                
                                <form action="/komentar/{{$value->id}}" method="POST">
                                    <a href="/komentar/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                                    @csrf
                                    @method('DELETE')
                                    <input type="submit" class="btn btn-danger my-1" value="Delete">
                                </form>
                            </td>
                        </tr>
                    @empty
                        <tr colspan="3">
                            <td>No data</td>
                        </tr>  
                    @endforelse              
                </tbody>
            </table>
    </div>
@endsection
@endsection