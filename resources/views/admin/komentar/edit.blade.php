@extends('admin.layout.master')
@section('title')
    Halaman Update Berita ({{$komentar->id}})
@endsection
@section('content')
<div class="m-5">
    <h2>Edit Data</h2>
    <div class="m-5">
        <h2>Edit Data</h2>
        
        <form action="/komentar/{{$komentar->id}}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('put')
            <div class="form-group">
                <label for="">judul Berita</label>
                <input disabled type="text" class="form-control"  id="title" value="{{$komentar->berita->judul}} ">
            </div>
            <div class="form-group">
                <label for="isi">Isi</label>
                <br>
                <textarea name="isi" id="" class="form-control" cols="30" rows="10">{{$komentar->isi}}</textarea>
                @error('isi')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>

            <button type="submit" class="btn btn-primary">Update</button>
        </form>
    </div>
@endsection