@extends('admin.layout.master')
@section('title')
    Halaman Tambah Berita
@endsection
@section('content')
        <div class="m-5">
            <h2>Tambah Data</h2>
            <form action="/komentar" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <label for="users_id">Penulis</label>
                    <h3>{{ Auth::users()->name }}</h3>
                    <input type="hidden" name="users_id" value="{{ Auth::users()->id }}">
                </div>
                <div class="form-group">
                    <label for="isi">Isi Komentar</label>
                    <br>
                    <textarea name="isi" id="" class="form-control" cols="30" rows="10"></textarea>
                    @error('isi')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                
                <div class="form-group">
                    <label for="berita_id">Berita</label>
                    <select name="berita_id" class="form-control" id="">
                        <option value="">Pilih Berita</option>
                        @foreach ($berita as $item)
                            <option value="{{$item->id}} ">{{$item->judul}}</option>
                        @endforeach
                    </select>
                    @error('berita_id')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <button type="submit" class="btn btn-primary">Tambah</button>
            </form>
        </div>
@endsection