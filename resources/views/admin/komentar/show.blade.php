@extends('admin.layout.master')
@section('title')
    Show Berita
@endsection
@section('content')
<a href="/admnews" class="btn btn-info">Back</a>
<div class="card mb-3 border mt-5" style="max-width: 50%;">
        <img src="{{asset('poster/' . $berita->poster )}}" class="card-img-top rounded" alt="...">
      <div class="col-md-8">
        <div class="card-body">
          <h5 class="card-title">{{$berita->judul}}</h5>
          <p class="card-text">{{$berita->isi}}</p>
        </div>
    </div>
  </div>

@endsection