@extends('layout.master')

@section('isi')
<section class="hero-wrap hero-wrap-2" style="background-image: url('images/bg_2.jpg');">
    <div class="overlay"></div>
    <div class="container">
      <div class="row no-gutters slider-text align-items-center justify-content-center">
        <div class="col-md-9 ftco-animate text-center">
          <h1 class="mb-2 bread">News</h1>
        </div>
      </div>
    </div>
  </section>

  <div class="row m-5">
    @forelse ($berita as $key=>$value)
    <div class="col-md-6 col-lg-4 ftco-animate">
      <div class="blog-entry">
        <a href="/news/{{$value->id}}" class="block-20 d-flex align-items-end rounded" style="background-image: url('{{asset('gambar/' . $value->gambar )}}');">
                          
        </a>
        <div class="text bg-white p-4">
          <h3 class="heading"><a href="/news/{{$value->id}}">{{$value->judul}}</a></h3>
          <p>{{Str::limit($value->isi,50)}} </p>
          <div class="d-flex align-items-center mt-4">
              <p class="mb-0"><a href="/news/{{$value->id}}" class="btn btn-secondary">Read More <span class="ion-ios-arrow-round-forward"></span></a></p>
              <p class="ml-auto mb-0">
                  <a href="#" class="mr-2">{{$value->users->name}}</a>
                  <a href="#" class="meta-chat"><span class="icon-chat"></span></a>
              </p>
          </div>
        </div>
      </div>
    </div>
    @empty
    <div class="col-md-6 col-lg-4 ftco-animate">
      <div class="blog-entry">
        <div class="text bg-white p-4">
          <h3 class="heading"><a href="#">No Content</a></h3>
          <p>No Content</p>
        </div>
      </div>
    </div>
    @endforelse
</div>
@endsection