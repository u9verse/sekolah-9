@extends('layout.master')

@section('isi')
<section class="home-slider owl-carousel">
    <div class="slider-item" style="background-image:url({{asset('template/images/bg_1.jpg')}});">
        <div class="overlay"></div>
      <div class="container">
        <div class="row no-gutters slider-text align-items-center justify-content-center" data-scrollax-parent="true">
        <div class="col-md-8 text-center ftco-animate">
          <h1 class="mb-4">Usia dini  <span>Seharusnya untuk mengeksplor dunia </span></h1>
          <p><a href="#" class="btn btn-secondary px-4 py-3 mt-3">Read More</a></p>
        </div>
      </div>
      </div>
    </div>

    <div class="slider-item" style="background-image:url({{asset('template/images/bg_2.jpg')}});">
        <div class="overlay"></div>
      <div class="container">
        <div class="row no-gutters slider-text align-items-center justify-content-center" data-scrollax-parent="true">
        <div class="col-md-8 text-center ftco-animate">
          <h1 class="mb-4">Cepat Belajar<span> Untuk anak-anak </span></h1>
          <p><a href="#" class="btn btn-secondary px-4 py-3 mt-3">Read More</a></p>
        </div>
      </div>
      </div>
    </div>
  </section>

  <section class="ftco-services ftco-no-pb">
          <div class="container-wrap">
              <div class="row no-gutters">
        <div class="col-md-3 d-flex services align-self-stretch pb-4 px-4 ftco-animate bg-primary">
          <div class="media block-6 d-block text-center">
            <div class="icon d-flex justify-content-center align-items-center">
                  <span class="flaticon-teacher"></span>
            </div>
            <div class="media-body p-2 mt-3">
              <h3 class="heading">Great Teacher</h3>
              <p>Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic.</p>
            </div>
          </div>      
        </div>
        <div class="col-md-3 d-flex services align-self-stretch pb-4 px-4 ftco-animate bg-tertiary">
          <div class="media block-6 d-block text-center">
            <div class="icon d-flex justify-content-center align-items-center">
                  <span class="flaticon-reading"></span>
            </div>
            <div class="media-body p-2 mt-3">
              <h3 class="heading">Kurikulum yang spesial</h3>
              <p>Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic.</p>
            </div>
          </div>    
        </div>
        <div class="col-md-3 d-flex services align-self-stretch pb-4 px-4 ftco-animate bg-fifth">
          <div class="media block-6 d-block text-center">
            <div class="icon d-flex justify-content-center align-items-center">
                  <span class="flaticon-books"></span>
            </div>
            <div class="media-body p-2 mt-3">
              <h3 class="heading">Book &amp; Perpustakaan</h3>
              <p>Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic.</p>
            </div>
          </div>      
        </div>
        <div class="col-md-3 d-flex services align-self-stretch pb-4 px-4 ftco-animate bg-quarternary">
          <div class="media block-6 d-block text-center">
            <div class="icon d-flex justify-content-center align-items-center">
                  <span class="flaticon-diploma"></span>
            </div>
            <div class="media-body p-2 mt-3">
              <h3 class="heading">Sertifikat</h3>
              <p>Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic.</p>
            </div>
          </div>      
        </div>
      </div>
          </div>
      </section>
      
      <section class="ftco-section bg-light">
        <div class="container">
            <div class="row justify-content-center mb-5 pb-2">
      <div class="col-md-8 text-center heading-section ftco-animate">
        <h2 class="mb-4"><span>Blog</span> Terkini</h2>
        <p>Separated they live in. A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country</p>
      </div>
    </div>
            <div class="row">
              
            @forelse ($berita as $key=>$value)
            <div class="col-md-6 col-lg-4 ftco-animate">
              <div class="blog-entry">
                <a href="/news/{{$value->id}}" class="block-20 d-flex align-items-end" style="background-image: url('{{asset('gambar/' . $value->gambar )}}');">
                                  
                </a>
                <div class="text bg-white p-4">
                  <h3 class="heading"><a href="/news/{{$value->id}}">{{$value->judul}}</a></h3>
                  <p>{{Str::limit($value->isi,50)}} </p>
                  <div class="d-flex align-items-center mt-4">
                      <p class="mb-0"><a href="/news/{{$value->id}}" class="btn btn-secondary">Read More <span class="ion-ios-arrow-round-forward"></span></a></p>
                      <p class="ml-auto mb-0">
                          <a href="#" class="mr-2">{{$value->users->name}}</a>
                          <a href="#" class="meta-chat"><span class="icon-chat"></span></a>
                      </p>
                  </div>
                </div>
              </div>
            </div>
            @empty
            <div class="col-md-6 col-lg-4 ftco-animate">
              <div class="blog-entry">
                <div class="text bg-white p-4">
                  <h3 class="heading"><a href="#">No Content</a></h3>
                  <p>No Content</p>
                </div>
              </div>
            </div>
            @endforelse
      
    
    </div>
        </div>
    </section>

      <section class="ftco-section ftco-no-pt ftc-no-pb">
          <div class="container">
              <div class="row">
                  <div class="col-md-5 order-md-last wrap-about py-5 wrap-about bg-light">
                      <div class="text px-4 ftco-animate">
                <h2 class="mb-4">Welcome to SD Mentari</h2>
                          <p>On her way she met a copy. The copy warned the Little Blind Text, that where it came from it would have been rewritten a thousand times and everything that was left from its origin would be the word.</p>
                          <p>Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean. A small river named Duden flows by their place and supplies it with the necessary regelialia. And if she hasn’t been rewritten, then they are still using her.</p>
                          <p><a href="#" class="btn btn-secondary px-4 py-3">Read More</a></p>
                      </div>
                  </div>
                  <div class="col-md-7 wrap-about py-5 pr-md-4 ftco-animate">
            <h2 class="mb-4">What We Offer</h2>
                      <p>On her way she met a copy. The copy warned the Little Blind Text, that where it came from it would have been rewritten a thousand times and everything that was left from its origin would be the word.</p>
                      <div class="row mt-5">
                          <div class="col-lg-6">
                              <div class="services-2 d-flex">
                                  <div class="icon mt-2 mr-3 d-flex justify-content-center align-items-center"><span class="flaticon-security"></span></div>
                                  <div class="text">
                                      <h3>Safety First</h3>
                                      <p>Far far away, behind the word mountains, far from the countries Vokalia.</p>
                                  </div>
                              </div>
                          </div>
                          <div class="col-lg-6">
                              <div class="services-2 d-flex">
                                  <div class="icon mt-2 mr-3 d-flex justify-content-center align-items-center"><span class="flaticon-reading"></span></div>
                                  <div class="text">
                                      <h3>Regular Classes</h3>
                                      <p>Far far away, behind the word mountains, far from the countries Vokalia.</p>
                                  </div>
                              </div>
                          </div>
                          <div class="col-lg-6">
                              <div class="services-2 d-flex">
                                  <div class="icon mt-2 mr-3 d-flex justify-content-center align-items-center"><span class="flaticon-diploma"></span></div>
                                  <div class="text">
                                      <h3>Certified Teachers</h3>
                                      <p>Far far away, behind the word mountains, far from the countries Vokalia.</p>
                                  </div>
                              </div>
                          </div>
                          <div class="col-lg-6">
                              <div class="services-2 d-flex">
                                  <div class="icon mt-2 mr-3 d-flex justify-content-center align-items-center"><span class="flaticon-education"></span></div>
                                  <div class="text">
                                      <h3>Sufficient Classrooms</h3>
                                      <p>Far far away, behind the word mountains, far from the countries Vokalia.</p>
                                  </div>
                              </div>
                          </div>
                          <div class="col-lg-6">
                              <div class="services-2 d-flex">
                                  <div class="icon mt-2 mr-3 d-flex justify-content-center align-items-center"><span class="flaticon-jigsaw"></span></div>
                                  <div class="text">
                                      <h3>Creative Lessons</h3>
                                      <p>Far far away, behind the word mountains, far from the countries Vokalia.</p>
                                  </div>
                              </div>
                          </div>
                          <div class="col-lg-6">
                              <div class="services-2 d-flex">
                                  <div class="icon mt-2 mr-3 d-flex justify-content-center align-items-center"><span class="flaticon-kids"></span></div>
                                  <div class="text">
                                      <h3>Sports Facilities</h3>
                                      <p>Far far away, behind the word mountains, far from the countries Vokalia.</p>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </section>
      
      <section class="ftco-intro" style="background-image: url({{asset('template/images/bg_3.jpg')}});" data-stellar-background-ratio="0.5">
          <div class="overlay"></div>
          <div class="container">
              <div class="row">
                  <div class="col-md-9">
                      <h2>Teaching Your Child Some Good Manners</h2>
                      <p class="mb-0">A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.</p>
                  </div>
                  <div class="col-md-3 d-flex align-items-center">
                      <p class="mb-0"><a href="#" class="btn btn-secondary px-4 py-3">Take a Course</a></p>
                  </div>
              </div>
          </div>
      </section>

      
      <section class="ftco-section ftco-no-pb">
          <div class="container">
              <div class="row justify-content-center mb-5 pb-2">
        <div class="col-md-8 text-center heading-section ftco-animate">
          <h2 class="mb-4"><span>Web</span> Development</h2>
          <p>Separated they live in. A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country</p>
        </div>
      </div>	
              <div class="row">
                  <div class="col-md-6 col-lg-4 ftco-animate">
                      <div class="staff">
                          <div class="img-wrap d-flex align-items-stretch">
                              <div class="img align-self-stretch" style="background-image: url({{asset('template/images/teacher-1.jpg')}});"></div>
                          </div>
                          <div class="text pt-3 text-center">
                              <h3>Richard</h3>
                              <span class="position mb-2">Web Development</span>
                              <div class="faded">
                                  <p>I am an ambitious workaholic, but apart from that, pretty simple person.</p>
                                  <ul class="ftco-social text-center">
                      <li class="ftco-animate"><a href="#"><span class="icon-twitter"></span></a></li>
                      <li class="ftco-animate"><a href="#"><span class="icon-facebook"></span></a></li>
                      <li class="ftco-animate"><a href="#"><span class="icon-google-plus"></span></a></li>
                      <li class="ftco-animate"><a href="#"><span class="icon-instagram"></span></a></li>
                    </ul>
                </div>
                          </div>
                      </div>
                  </div>
                  <div class="col-md-6 col-lg-4 ftco-animate">
                      <div class="staff">
                          <div class="img-wrap d-flex align-items-stretch">
                              <div class="img align-self-stretch" style="background-image: url({{asset('template/images/teacher-2.png')}});"></div>
                          </div>
                          <div class="text pt-3 text-center">
                              <h3>Albert</h3>
                              <span class="position mb-2">Web Development</span>
                              <div class="faded">
                                  <p>Wanna be Jakartan</p>
                                  <ul class="ftco-social text-center">
                      <li class="ftco-animate"><a href="#"><span class="icon-twitter"></span></a></li>
                      <li class="ftco-animate"><a href="#"><span class="icon-facebook"></span></a></li>
                      <li class="ftco-animate"><a href="#"><span class="icon-google-plus"></span></a></li>
                      <li class="ftco-animate"><a href="#"><span class="icon-instagram"></span></a></li>
                    </ul>
                </div>
                          </div>
                      </div>
                  </div>
                  <div class="col-md-6 col-lg-4 ftco-animate">
                      <div class="staff">
                          <div class="img-wrap d-flex align-items-stretch">
                              <div class="img align-self-stretch" style="background-image: url({{asset('template/images/teacher-4.jpg')}});"></div>
                          </div>
                          <div class="text pt-3 text-center">
                              <h3>Kennedy</h3>
                              <span class="position mb-2">Web Development</span>
                              <div class="faded">
                                  <p>Menjadi kuat agar menindas yang lemah</p>
                                  <ul class="ftco-social text-center">
                      <li class="ftco-animate"><a href="#"><span class="icon-twitter"></span></a></li>
                      <li class="ftco-animate"><a href="#"><span class="icon-facebook"></span></a></li>
                      <li class="ftco-animate"><a href="#"><span class="icon-google-plus"></span></a></li>
                      <li class="ftco-animate"><a href="#"><span class="icon-instagram"></span></a></li>
                    </ul>
                </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </section>

      <section class="ftco-section">
          <div class="container">
              <div class="row justify-content-center mb-5 pb-2">
        <div class="col-md-8 text-center heading-section ftco-animate">
          <h2 class="mb-4"><span>Our</span> Courses</h2>
          <p>Separated they live in. A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country</p>
        </div>
      </div>	
              <div class="row">
                  <div class="col-md-6 course d-lg-flex ftco-animate">
                      <div class="img" style="background-image: url({{asset('template/images/course-1.jpg')}});"></div>
                      <div class="text bg-light p-4">
                          <h3><a href="#">Arts Lesson</a></h3>
                          <p class="subheading"><span>Class time:</span> 9:00am - 10am</p>
                          <p>Separated they live in. A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country</p>
                      </div>
                  </div>
                  <div class="col-md-6 course d-lg-flex ftco-animate">
                      <div class="img" style="background-image: url({{asset('template/images/course-2.jpg')}});"></div>
                      <div class="text bg-light p-4">
                          <h3><a href="#">Language Lesson</a></h3>
                          <p class="subheading"><span>Class time:</span> 9:00am - 10am</p>
                          <p>Separated they live in. A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country</p>
                      </div>
                  </div>
                  <div class="col-md-6 course d-lg-flex ftco-animate">
                      <div class="img" style="background-image: url({{asset('template/images/course-3.jpg')}});"></div>
                      <div class="text bg-light p-4">
                          <h3><a href="#">Music Lesson</a></h3>
                          <p class="subheading"><span>Class time:</span> 9:00am - 10am</p>
                          <p>Separated they live in. A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country</p>
                      </div>
                  </div>
                  <div class="col-md-6 course d-lg-flex ftco-animate">
                      <div class="img" style="background-image: url({{asset('template/images/course-4.jpg')}});"></div>
                      <div class="text bg-light p-4">
                          <h3><a href="#">Sports Lesson</a></h3>
                          <p class="subheading"><span>Class time:</span> 9:00am - 10am</p>
                          <p>Separated they live in. A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country</p>
                      </div>
                  </div>
              </div>
          </div>
      </section>

      <section class="ftco-section ftco-counter img" id="section-counter" style="background-image: url({{asset('template/images/bg_4.jpg')}});" data-stellar-background-ratio="0.5">
      <div class="container">
          <div class="row justify-content-center mb-5 pb-2">
        <div class="col-md-8 text-center heading-section heading-section-black ftco-animate">
          <h2 class="mb-4"><span>20 Years of</span> Experience</h2>
          <p>Separated they live in. A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country</p>
        </div>
      </div>	
          <div class="row d-md-flex align-items-center justify-content-center">
              <div class="col-lg-10">
                  <div class="row d-md-flex align-items-center">
                <div class="col-md d-flex justify-content-center counter-wrap ftco-animate">
                  <div class="block-18">
                      <div class="icon"><span class="flaticon-doctor"></span></div>
                    <div class="text">
                      <strong class="number" data-number="18">0</strong>
                      <span>Certified Teachers</span>
                    </div>
                  </div>
                </div>
                <div class="col-md d-flex justify-content-center counter-wrap ftco-animate">
                  <div class="block-18">
                      <div class="icon"><span class="flaticon-doctor"></span></div>
                    <div class="text">
                      <strong class="number" data-number="351">0</strong>
                      <span>Successful Kids</span>
                    </div>
                  </div>
                </div>
                <div class="col-md d-flex justify-content-center counter-wrap ftco-animate">
                  <div class="block-18">
                      <div class="icon"><span class="flaticon-doctor"></span></div>
                    <div class="text">
                      <strong class="number" data-number="564">0</strong>
                      <span>Happy Parents</span>
                    </div>
                  </div>
                </div>
                <div class="col-md d-flex justify-content-center counter-wrap ftco-animate">
                  <div class="block-18">
                      <div class="icon"><span class="flaticon-doctor"></span></div>
                    <div class="text">
                      <strong class="number" data-number="300">0</strong>
                      <span>Awards Won</span>
                    </div>
                  </div>
                </div>
            </div>
        </div>
      </div>
      </div>
  </section>

  <section class="ftco-section testimony-section bg-light">
    <div class="container">
      <div class="row justify-content-center mb-5 pb-2">
        <div class="col-md-8 text-center heading-section ftco-animate">
          <h2 class="mb-4"><span>What Parents</span> Says About Us</h2>
          <p>Separated they live in. A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country</p>
        </div>
      </div>
      <div class="row ftco-animate justify-content-center">
        <div class="col-md-12">
          <div class="carousel-testimony owl-carousel">
            <div class="item">
              <div class="testimony-wrap d-flex">
                <div class="users-img mr-4" style="background-image: url({{asset('template/images/person_1.jpg')}})">
                </div>
                <div class="text ml-2 bg-light">
                    <span class="quote d-flex align-items-center justify-content-center">
                    <i class="icon-quote-left"></i>
                  </span>
                  <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
                  <p class="name">Mr. Racky Henderson</p>
                  <span class="position">Father</span>
                </div>
              </div>
            </div>
            <div class="item">
              <div class="testimony-wrap d-flex">
                <div class="users-img mr-4" style="background-image: url({{asset('template/images/teacher-8.jpg')}})">
                </div>
                <div class="text ml-2 bg-light">
                    <span class="quote d-flex align-items-center justify-content-center">
                    <i class="icon-quote-left"></i>
                  </span>
                  <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
                  <p class="name">Mrs. Henry Dee</p>
                  <span class="position">Mother</span>
                </div>
              </div>
            </div>
            <div class="item">
              <div class="testimony-wrap d-flex">
                <div class="users-img mr-4" style="background-image: url({{asset('template/images/teacher-3.jpg')}})">
                </div>
                <div class="text ml-2 bg-light">
                    <span class="quote d-flex align-items-center justify-content-center">
                    <i class="icon-quote-left"></i>
                  </span>
                  <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
                  <p class="name">Mrs. Mark Huff</p>
                  <span class="position">Mother</span>
                </div>
              </div>
            </div>
            <div class="item">
              <div class="testimony-wrap d-flex">
                <div class="users-img mr-4" style="background-image: url({{asset('template/images/teacher-7.jpg')}})">
                </div>
                <div class="text ml-2 bg-light">
                    <span class="quote d-flex align-items-center justify-content-center">
                    <i class="icon-quote-left"></i>
                  </span>
                  <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
                  <p class="name">Mrs. Rodel Golez</p>
                  <span class="position">Mother</span>
                </div>
              </div>
            </div>
            <div class="item">
              <div class="testimony-wrap d-flex">
                <div class="users-img mr-4" style="background-image: url({{asset('template/images/teacher-6.jpg')}})">
                </div>
                <div class="text ml-2 bg-light">
                    <span class="quote d-flex align-items-center justify-content-center">
                    <i class="icon-quote-left"></i>
                  </span>
                  <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
                  <p class="name">Mrs. Ken Bosh</p>
                  <span class="position">Mother</span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

<section class="ftco-gallery">
    <div class="container-wrap">
        <div class="row no-gutters">
                <div class="col-md-3 ftco-animate">
                    <a href="images/image_1.jpg" class="gallery image-popup img d-flex align-items-center" style="background-image: url({{asset('template/images/course-1.jpg')}});">
                        <div class="icon mb-4 d-flex align-items-center justify-content-center">
                        <span class="icon-instagram"></span>
                    </div>
                    </a>
                </div>
                <div class="col-md-3 ftco-animate">
                    <a href="images/image_2.jpg" class="gallery image-popup img d-flex align-items-center" style="background-image: url({{asset('template/images/image_2.jpg')}});">
                        <div class="icon mb-4 d-flex align-items-center justify-content-center">
                        <span class="icon-instagram"></span>
                    </div>
                    </a>
                </div>
                <div class="col-md-3 ftco-animate">
                    <a href="images/image_3.jpg" class="gallery image-popup img d-flex align-items-center" style="background-image: url({{asset('template/images/image_3.jpg')}});">
                        <div class="icon mb-4 d-flex align-items-center justify-content-center">
                        <span class="icon-instagram"></span>
                    </div>
                    </a>
                </div>
                <div class="col-md-3 ftco-animate">
                    <a href="images/image_4.jpg" class="gallery image-popup img d-flex align-items-center" style="background-image: url({{asset('template/images/image_4.jpg')}});">
                        <div class="icon mb-4 d-flex align-items-center justify-content-center">
                        <span class="icon-instagram"></span>
                    </div>
                    </a>
                </div>
    </div>
    </div>
</section>
@endsection