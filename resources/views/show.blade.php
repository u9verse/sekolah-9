@extends('layout.master')
@section('isi')

<section class="hero-wrap hero-wrap-2" style="background-image: url('images/bg_2.jpg');">
    <div class="overlay"></div>
    <div class="container">
      <div class="row no-gutters slider-text align-items-center justify-content-center">
        <div class="col-md-9 ftco-animate text-center">
          <h1 class="mb-2 bread">{{$berita->judul}}</h1>
        </div>
      </div>
    </div>
  </section>

<div class="container">
    <a href="/news" class="btn btn-info m-5">Back</a>
    <div class="card mb-3 border m-5 " style="max-width: 80%;">
        <img src="{{asset('gambar/' . $berita->gambar )}}" class="card-img-top rounded" alt="...">
      <div class="col-md-8">
        <div class="card-body">
          <h5 class="card-title">{{$berita->judul}}</h5>
          <p class="card-text">{{$berita->isi}}</p>
        </div>
    </div>
  </div>
</div>
  <div class="container">
        @foreach ($komentar as $item)
        <div class="card mb-3 border m-5" style="width: 80%;;">
            <div class="card-body">
              <p class="card-text">{{$item->isi}}</p>
              <p class=".text-primary">{{$item->users->name}}</p>
            </div>
          </div>
        @endforeach
    </div>

@endsection