# Final project
## Kelompok 9
## Anggota kelompok
* Felix Bagus
* Hakim Amal Adni
* Leon Prasetyo
## Tema project
Portal berita sekolah
## ERD
![ERD](https://gitlab.com/u9verse/sekolah-9/-/raw/master/ERD.png)
## Link video
Link demo aplikasi: Link video demo yang berisi penjelasan singkat mengenai project yang dikerjakan (template yang dipakai, demo web)