<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Komentar extends Model
{
    public $timestamps = false;
    protected $table ="komentar";
    protected $fillable = ['isi','users_id','berita_id'];
    public function berita()
    {
        return $this->belongsTo('App\Berita');
    }
    public function users()
    {
        return $this->belongsTo('App\User');
    }
}
