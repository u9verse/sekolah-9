<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    public $timestamps = false;
    protected $table ="tag";
    protected $fillable = ['id','nama'];

    public function berita()
    {
        return $this->belongsTo('App\berita');
    }
}
