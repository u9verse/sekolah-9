<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Berita;
use File;
class BeritaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $berita = Berita::all();
        return view('admin.berita.index', compact('berita'));  
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = DB::table('user')->get();
        $tag = DB::table('tag')->get();
        return view('admin.berita.create', compact('user','tag'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
    		'judul' => 'required',
    		'isi' => 'required',
    		'gambar' => 'mimes:jpeg,jpg,png|max:2200',
            'tag_id' => 'required',
    		'users_id' => 'required'
    	]);

        $gambar = $request->gambar;
        $new_gambar = time() . ' - ' . $gambar->getClientOriginalName();
 
        Berita::create([
    		'judul' => $request->judul,
    		'isi' => $request->isi,
            'gambar' => $new_gambar,
    		'tag_id' => $request->tag_id,
            'users_id' => $request->users_id
    	]);

        $gambar->move('gambar/', $new_gambar);
 
    	return redirect('/admnews');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $berita = Berita::find($id);
        return view('admin.berita.show', compact('berita'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $berita = Berita::find($id);
        $profile = DB::table('profile')->get();
        $tag = DB::table('tag')->get();
        return view('admin.berita.edit', compact('berita','tag','profile'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'judul' => 'required',
    		'isi' => 'required',
    		'poster' => 'mimes:jpeg,jpg,png|max:2200',
        ]);

        $berita = Berita::findorfail($id);

        if ($request->has('poster')) {
            //delete image
            $path = "poster/";
            File::delete($path . $berita->poster);
            $poster = $request->poster;
            $new_poster = time() . ' - ' . $poster->getClientOriginalName();
            $poster->move('poster/', $new_poster);
            $data_berita = [
            'judul' => $request->judul,
    		'isi' => $request->isi,
            'poster' => $new_poster
            ];
        }else {
            $data_berita = [
            'judul' => $request->judul,
    		'isi' => $request->isi
            ];
        }
        $berita->update($data_berita);

        return redirect('/admnews');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $berita = Berita::findorfail($id);
        $berita->delete();

        $path = "poster/";
        File::delete($path . $berita->poster);

        return redirect('/admnews');
    }
}
