<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Berita;
use App\User;
use App\Tag;
use App\Komentar;

class MainController extends Controller
{
    public function index(){
        
        $users = users::all();
        $berita = Berita::all();
        return view('/welcome', compact('berita'));
    }

    public function news(){
        
        $users = users::all();
        $berita = Berita::all();
        return view('/news', compact('berita'));
    }

    public function show($id){
        
        $users = users::all();
        $komentar = Komentar::all();
        $berita = DB::table('berita')->where('id', $id)->first();
        return view('/show', compact('berita','komentar'));
    }
}
