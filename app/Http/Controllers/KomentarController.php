<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Komentar;
use File;
class KomentarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $komentar = Komentar::all();
        return view('admin.komentar.index', compact('komentar'));  
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = DB::table('user')->get();
        $berita = DB::table('berita')->get();
        return view('admin.komentar.create', compact('user','berita'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
    		'isi' => 'required',
            'berita_id' => 'required',
    		'users_id' => 'required'
    	]);
 
        Komentar::create([
    		'isi' => $request->isi,
    		'berita_id' => $request->berita_id,
            'users_id' => $request->users_id
    	]);
 
    	return redirect('/komentar');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    { //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $komentar = Komentar::find($id);
        $users = DB::table('user')->get();
        $berita = DB::table('berita')->get();
        return view('admin.komentar.edit', compact('berita','user','komentar'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
    		'isi' => 'required',
        ]);

        $komentar = Komentar::findorfail($id);
        $komentar->update([
            'isi' => $request->isi]);
        return redirect('/komentar');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $berita = Berita::findorfail($id);
        $berita->delete();

        $path = "poster/";
        File::delete($path . $berita->poster);

        return redirect('/admnews');
    }
}
