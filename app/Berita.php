<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Berita extends Model
{
    protected $table ="berita";
    protected $fillable = ['id','gambar','judul','isi','created_at','updated_at','tag_id','users_id'];

    public function tag()
    {
        return $this->belongsTo('App\Tag');
    }

    public function users()
    {
        return $this->belongsTo('App\User');
    }

    public function komentar()
    {
        return $this->hasMany('App\Komentar');
    }
}
