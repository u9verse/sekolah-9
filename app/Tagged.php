<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tagged extends Model
{
    protected $table ="berita_has_tag";
    protected $fillable = ['berita_id','tag_id'];

    public function berita()
    {
        return $this->belongsTo('App\berita');
    }

    public function tag()
    {
        return $this->belongsTo('App\tag');
    }
}