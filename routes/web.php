<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

route::group(['middleware' => ['auth']], function () {
    Route::get('/admin', function () {
        return view('admin.index');
    });
    Route::resource('komentar', 'KomentarController');
    Route::resource('tag', 'TagController');
    Route::resource('admnews', 'BeritaController');
});
Route::get('/', 'MainController@index');
Route::get('/news', 'MainController@news');
Route::get('/news/{news_id} ', 'MainController@show');

Auth::routes();

